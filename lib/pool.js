'use strict';

const Abstract = require('@scola/transport-pool');
const Error = require('@scola/error');

class RouterPool extends Abstract.Pool {
  handleConnection(connection) {
    this.bindConnectionListeners(connection);
    this.emit('connection', connection);
  }

  handleClose(connection, code, reason) {
    this.unbindConnectionListeners(connection);
    this.emit('close', connection, code, reason);
  }

  send(message) {
    super.send(message);

    const connection = message.getConnection();

    if (connection.canSend()) {
      connection.send(message);
      return this;
    }

    return this.emit('error', new Error('transport_message_not_sent', {
      detail: {
        message
      }
    }));
  }
}

module.exports = RouterPool;
